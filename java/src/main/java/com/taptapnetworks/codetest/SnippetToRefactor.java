package com.taptapnetworks.codetest;

public class SnippetToRefactor
{
	public static final String SMS = "SMS";
	public static final String MAIL = "MAIL";
	public static final String SMPP_IP = "192.168.1.4";
	public static final String SMPP_PORT = "4301";
	public static final String MAIL_SERVER = "mail.myserver.com";
	public static final String USERNAME = "your-username";
	public static final String PASSWORD = "your-password";

	/**
	 * Send a message with a subject to an address.
	 * 
	 * @param type
	 *            SMS or MAIL
	 * @param message
	 *            message to be sent
	 * @param to
	 *            to
	 * @param subject
	 *            email's subject, null for SMS
	 * @return true if succeeded, false otherwise
	 */
	public Boolean send(String type, String message, String to, String subject)
	{
		Boolean result = false;
		if (type == null || type.isEmpty())
		{
			throw new RuntimeException("Type is null");
		} else if (message == null || message.isEmpty())
		{
			throw new RuntimeException("Message is null");
		} else if (to == null || to.isEmpty())
		{
			throw new RuntimeException("To is null");
		} else
		{
			if (type.equals(SMS))
			{
				SMPP smpp = new SMPP(SMPP_IP, SMPP_PORT);
				smpp.openConnection(USERNAME, PASSWORD);
				smpp.send(to, message);
				smpp.closeConnection();
				result = true;
			} else if (type.equals(MAIL))
			{
				SendMailConnection smc = new SendMailConnection(MAIL_SERVER);
				smc.prepareMessage(to, message, subject);
				smc.send();
				smc.close();
				result = true;
			} else
			{
				throw new RuntimeException("Type is unknown");
			}
		}
		return result;
	}
}
