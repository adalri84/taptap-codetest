package com.taptapnetworks.codetest;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for SnippetToRefactor.
 */
public class SnippetToRefactorTest extends TestCase
{
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public SnippetToRefactorTest(String testName)
	{
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite()
	{
		return new TestSuite(SnippetToRefactorTest.class);
	}

	/**
	 * Type null
	 */
	public void testTypeNull()
	{
		try
		{
			SnippetToRefactor snippet = new SnippetToRefactor();
			snippet.send(null, "message", "to", "subject");
		} catch (RuntimeException e)
		{
			assertEquals("Type is null", e.getMessage());
		}
	}

	/**
	 * Type empty
	 */
	public void testTypeEmpty()
	{
		try
		{
			SnippetToRefactor snippet = new SnippetToRefactor();
			snippet.send("", "message", "to", "subject");
		} catch (RuntimeException e)
		{
			assertEquals("Type is null", e.getMessage());
		}
	}

	/**
	 * Message null
	 */
	public void testMessageNull()
	{
		try
		{
			SnippetToRefactor snippet = new SnippetToRefactor();
			snippet.send("type", null, "to", "subject");
		} catch (RuntimeException e)
		{
			assertEquals("Message is null", e.getMessage());
		}
	}

	/**
	 * Message empty
	 */
	public void testMessageEmpty()
	{
		try
		{
			SnippetToRefactor snippet = new SnippetToRefactor();
			snippet.send("type", "", "to", "subject");
		} catch (RuntimeException e)
		{
			assertEquals("Message is null", e.getMessage());
		}
	}

	/**
	 * To null
	 */
	public void testToNull()
	{
		try
		{
			SnippetToRefactor snippet = new SnippetToRefactor();
			snippet.send("type", "message", null, "subject");
		} catch (RuntimeException e)
		{
			assertEquals("To is null", e.getMessage());
		}
	}

	/**
	 * To empty
	 */
	public void testToEmpty()
	{
		try
		{
			SnippetToRefactor snippet = new SnippetToRefactor();
			snippet.send("type", "message", "", "subject");
		} catch (RuntimeException e)
		{
			assertEquals("To is null", e.getMessage());
		}
	}

	/**
	 * Type unknown
	 */
	public void testTypeUnknown()
	{
		try
		{
			SnippetToRefactor snippet = new SnippetToRefactor();
			snippet.send("type", "message", "to", "subject");
		} catch (RuntimeException e)
		{
			assertEquals("Type is unknown", e.getMessage());
		}
	}

	/**
	 * Type SMS
	 */
	public void testTypeSMS()
	{
		SnippetToRefactor snippet = new SnippetToRefactor();
		assertTrue(snippet.send("SMS", "message", "to", "subject"));
	}

	/**
	 * Type MAIL
	 */
	public void testTypeMAIL()
	{
		SnippetToRefactor snippet = new SnippetToRefactor();
		assertTrue(snippet.send("MAIL", "message", "to", "subject"));
	}
}